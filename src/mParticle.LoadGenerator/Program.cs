﻿using System;
using System.Net;
//Added these 3 dependencies
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
 
namespace mParticle.LoadGenerator
{
    public sealed class Program
    {
        //class to hold request payload elements
        public class RequestPayload
        {
            public string name { get; set; }
            public DateTime date { get; set; }
            public int requests_sent { get; set; }
        };

        public static void Main(string[] args)
        {
            string configFile = "config.json";
            if (args.Length > 0)
            {
                configFile = args[0];
            }

            Config config = Config.GetArguments(configFile);
            if (config == null)
            {
                Console.WriteLine("Failed to parse configuration.");
                return;
            }

            // TODO Do some work!
            //Do this stuff once
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(config.ServerURL);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("X-Api-Key", config.AuthKey);

            //High-level variables
            DateTime programStart = DateTime.Now;
            int totalRequests = 10;

            //Set request variables - this should probably be included in SendRequestAsync() 
            RequestPayload requestPayload = new RequestPayload
            {
                name = config.UserName,
                date = DateTime.UtcNow,
                requests_sent = totalRequests
            };

            //invoke all methods
            RunRequestAsync(requestPayload, client, totalRequests).GetAwaiter().GetResult();

            //Write summary variables - this could be abstracted
            Console.WriteLine("Program Start: " + programStart);
            DateTime programEnd = DateTime.Now;
            Console.WriteLine("Program End: " + programEnd);
            TimeSpan programDuration = programEnd.Subtract(programStart);
            Console.WriteLine("Program Duration (Seconds): " + programDuration.Seconds);
            Console.WriteLine("Total Requested: " + totalRequests);
            //This is hacky
            Console.WriteLine("Avg Elapsed Time: " + programDuration.Seconds / totalRequests);
        }

        static async Task<HttpResponseMessage> SendRequestAsync(RequestPayload requestPayload, HttpClient client)
        {
            var stringContent = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(requestPayload));

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, client.BaseAddress);
            request.Headers.Add("Accept", "application/json");
            request.Content = new StringContent(stringContent.ToString());
            request.Headers.Authorization = client.DefaultRequestHeaders.Authorization;
            
            HttpResponseMessage response = await client.PostAsync(request.RequestUri, request.Content);

            //Placeholder for Http error handling

            return response;
        }

        static void ShowResponse(HttpContent response, int i, TimeSpan requestDuration)
        {
            //Console.WriteLine("!" + response + "!");
            Console.WriteLine("Current Request #: " + i);
            Console.WriteLine("Request Duration (Seconds):" + requestDuration.Seconds);
        }

        static async Task RunRequestAsync(RequestPayload requestPayload, HttpClient client, int totalRequests)
        {
            int i = 1;
            int n = totalRequests;
            DateTime requestStart = DateTime.Now;
            DateTime requestEnd = DateTime.Now;
            TimeSpan requestDuration = default(TimeSpan);
            
            while (i <= n)
            { 
            requestStart = DateTime.Now;
            HttpResponseMessage response = await SendRequestAsync(requestPayload, client);
            requestEnd = DateTime.Now;
            requestDuration = requestEnd.Subtract(requestStart);
            ShowResponse(response.Content, i, requestDuration);
            i = i + 1;
            }
        }
    }
}
